package com.boot.security.server.wordTemplate.controller;



@Controller
@RequestMapping("/wordTmplate")
public class WordTempplateController {

	@RequestMapping(value = "/word", method = RequestMethod.GET)
	@ResponseBody
	public void getWord(String path, HttpServletRequest request, HttpServletResponse response) {
		try {
			String url = "D:\\abc.docx";
			File file = new File(url);
			String l = request.getRealPath("/") + "/" + url;
			String filename = file.getName();
			InputStream fis = new BufferedInputStream(new FileInputStream(file));
			byte[] buffer = new byte[fis.available()];
			fis.read(buffer);
			fis.close();
			response.reset();
			// 设置response的Header
			response.addHeader("Content-Length", "" + file.length());
			response.setContentType("application/x-msdownload;");

			OutputStream toClient = new BufferedOutputStream(response.getOutputStream());
			toClient.write(buffer);
			toClient.flush();
			toClient.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	
	}
	
}
