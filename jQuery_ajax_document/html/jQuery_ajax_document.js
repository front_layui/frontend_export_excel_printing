/**
 * Ajax下载服务器文件流的方法
 * @date 2020-01-20
 * @author quyunde
 */
;
(function($) {
	$.extend({
		ajaxDocument: function(option) {

		/**
             * option：配置项
             * {
             *     url,地址
             *     type, 请求类型ＰＯＳＴ，ＧＥＴ
             *     header，请求头token等设置
             *     param 参数
             *
             *  }
             */

			debugger;
			if(!option) {
				throw "param is error or NULL!";
			}
			if(!option.url) {
				throw "url is NULL or length=0!";
			}
			if(!option.type) {
				throw "type is NULL!";
			}

			const promise = new Promise(function(resolve, reject) {
				const handler = function() {
					if(4 !== this.readyState) {
						return false;
					}
					if(200 === this.status) {
						resolve(this.response);
					} else {
						reject(new Error(this.statusText));
					}
				};
				const client = new XMLHttpRequest();
				client.open(option.type, option.url, true);
				client.onreadystatechange = handler;
				client.responseType = "blob";

				if(option.header) {
					for(key in option.header) {
						client.setRequestHeader(key, option.header[key]);
					}
				}
				client.send(option.param);

			});
			return promise;
		}

	});

})(jQuery);